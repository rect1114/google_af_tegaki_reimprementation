import numpy as np
import layer as L
from collections import OrderedDict

class Network:
    def __init__(self,input_size,hidden_size,output_size,weight_std=0.01):
        self.params = {}
        self.params['Theta1'] = weight_std * np.random.randn(input_size,hidden_size)
        self.params['bias1'] = np.ones((hidden_size,1))
        self.params['Theta2'] = weight_std * np.random.randn(hidden_size,output_size)
        self.params['bias2'] = np.ones((output_size,1))

        self.layers = OrderedDict()
        self.layers['Affine1'] = L.Affine(self.params['Theta1'],self.params['bias1'])
        self.layers['Activation1'] = L.Sigmoid()
        self.layers['Affine2'] = L.Affine(self.params['Theta2'],self.params['bias2'])
        self.layers['Activation2'] = L.Sigmoid()
        
        self.lossLayer = L.Cross_Entropy()

    def predict(self,x):
        for layer in self.layers.values():
            x = layer.forward(x)

        return x


    def loss(self,x,t):
        y = self.predict(x)
        return self.lossLayer.forward(y,t)

    def gradient(self,x,t):
        self.loss(x,t)
        
        dout = 1
        dout = self.lossLayer.backward(dout)
        layers = list(self.layers.values())

        layers.reverse()

        for layer in layers:
            dout = layer.backward(dout)

        grads = {}
        grads['Theta1'] = self.layers['Affine1'].dTheta
        grads['bias1'] = self.layers['Affine1'].dbias
        grads['Theta2'] = self.layers['Affine2'].dTheta
        grads['bias2'] = self.layers['Affine2'].dbias

        return grads
