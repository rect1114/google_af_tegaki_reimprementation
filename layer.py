import numpy as np

class Sigmoid:
    def __init__(self):
        self.out = None

    def forward(self,x):
        out = 1 / (1 + np.exp(-x))
        self.out = out
        return out

    def backward(self,dout):
        dx = dout * (1.0 - self.out) * self.out
        return dx

class Cross_Entropy:
    def __init__(self):
        self.y = None
        self.t = None

    def forward(self,y,t):
        self.y = y
        self.t = t
        out = -np.sum(t * np.log(y))
        return out

    def backward(self,dout):
        return dout * (self.y - self.t)

class Affine:
    def __init__(self,Theta,bias):
        self.Theta = Theta
        self.bias = bias

        self.x = None
        self.dTheta = None
        self.dbias = None

    def forward(self,x):
        self.x = x
        out = np.dot(self.Theta.T,x) + self.bias

        return out

    def backward(self,dout):
        dx = np.dot(self.Theta,dout)
        self.dTheta = np.dot(dout,self.x.T)
        self.dbas = np.sum(dout,axis=0)

        return dx