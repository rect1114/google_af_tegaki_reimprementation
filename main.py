import numpy as np
from network import Network

def main():
    input_size = 64
    hidden_size = 20
    output_size = 10
    network = Network(input_size,hidden_size,output_size)

    x = convertTestData(input(">"))

    y = network.predict(x)

    print(np.argmax(y))

    t = convertTeachingData(input())


    grad = network.gradient(x,t)
    print(grad['Theta1'])

    


def convertTestData(raw_text,input_size=64):
    x = np.zeros((input_size,1))
    raw_text = raw_text.upper()
    x_temp = np.array(list(map(ord,raw_text))) - 33
    x[x_temp] = 1
    return x

def convertTeachingData(raw_text,input_size=10):
    return np.eye(input_size)[int(raw_text)].reshape(input_size,1)

if __name__ == "__main__":
    main()